package com.example.student.repository;

import com.example.student.model.StudentModel;
import com.example.student.model.StudentModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface studentRepository extends MongoRepository<StudentModel, Integer> {
    @Query("{rollno : ?0}")
    StudentModel findStudentByRoll(int rollno);

    @Query(value = "{rollno : ?0}", delete = true)
    void deleteStudentByRoll(int rollno);
}
