package com.example.student.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

@Document(collection = "studentInfo")

public class StudentModel {
    @Id
    public String id;
    public String name;
    @Indexed(unique=true)
    public int rollno;
    public long phn;
}