package com.example.student.controller;

import com.example.student.model.StudentModel;
import com.example.student.model.loginModel;
import com.example.student.model.StudentModel;
import com.example.student.repository.studentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {

    @Autowired
    private studentRepository studRepo;
    private StudentModel stud;
    private StudentModel login;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getStudents")
    public List<StudentModel> getStudents() {
        return studRepo.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getStudent")
    public StudentModel getStudent(@RequestParam(value = "rollno") int  rollno) {
        Optional <StudentModel> opt;
        opt = Optional.ofNullable(studRepo.findStudentByRoll(rollno));
        if(opt.isPresent()) {
            return (opt.get());
        }
        else {
            System.out.println("no student found");
        }
        return opt.get();
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/get")
    public StudentModel getstudent(@RequestParam(value = "rollno") int rollno)
    {
        return studRepo.findStudentByRoll(rollno);

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/student")
    public String updateName(@RequestBody StudentModel student){
        Optional<StudentModel> opt;
        opt = Optional.ofNullable(studRepo.findStudentByRoll(student.rollno));
        if(opt.isPresent()) {
            StudentModel a = opt.get();
            a.name = student.name;
            a.phn = student.phn;
            studRepo.save(a);
            return "student updated";
        }
        else {
            studRepo.save(student);
            return "Student does not exists";
        }
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/deleteStudent")
    public String deleteStudent(@RequestParam("rollno") int rollno){
        studRepo.deleteStudentByRoll(rollno);
        return "deleted successfully";
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/login")
    public boolean login(@RequestBody loginModel login){
        if(login.name.equals("admin") && login.password.equals("admin") ){
            return true;
        }
        else {
            return false;
        }
    }
}